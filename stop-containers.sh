prefix=indicafix

repositoriesPath=( 
gateway-svc
answer-svc
answer-web
auth-svc
auth-web
customer-svc
customer-web 
survey-svc
survey-web )

for repositoryPath in "${repositoriesPath[@]}"
do
  cd "../$prefix-$repositoryPath" && docker-compose down
done

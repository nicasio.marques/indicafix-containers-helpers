cd ..

repositories=( 
https://gitlab.com/ideafix_/indicafix/backend/indicafix-answer-svc 
https://gitlab.com/ideafix_/indicafix/frontend/indicafix-answer-web 
https://gitlab.com/ideafix_/indicafix/backend/indicafix-auth-svc  
https://gitlab.com/ideafix_/indicafix/frontend/indicafix-auth-web  
https://gitlab.com/ideafix_/indicafix/frontend/indicafix-components  
https://gitlab.com/ideafix_/indicafix/backend/indicafix-customer-svc  
https://gitlab.com/ideafix_/indicafix/frontend/indicafix-customer-web 
https://gitlab.com/ideafix_/indicafix/backend/indicafix-gateway-svc 
https://gitlab.com/ideafix_/indicafix/backend/indicafix-survey-svc 
https://gitlab.com/ideafix_/indicafix/frontend/indicafix-survey-web )

for repository in "${repositories[@]}"
do
  git clone "$repository"
done

cd indicafix-containers-helpers

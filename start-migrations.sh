#!/bin/bash
prefix=indicafix

repositoriesPath=( 
answer-svc
customer-svc
survey-svc 
)

for repositoryPath in "${repositoriesPath[@]}"
do
  cd "../$prefix-$repositoryPath" && docker-compose exec $repositoryPath rails db:prepare
done
